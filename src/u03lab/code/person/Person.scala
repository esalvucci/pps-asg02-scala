// SEC 6: SUM TYPES

  // Sum type: a sealed base trait, and cases extending it
  sealed trait Person //sealed: no other impl. except Student, Teacher
  case class Student(name: String, year: Int) extends Person
  case class Teacher(name: String, course: String) extends Person

  def name(p: Person): String = p match {
    case Student(n,_) => n
    case Teacher(n,_) => n
  }

  println(name(Student("mario",2015)))

