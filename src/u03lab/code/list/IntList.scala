// A LinkedList of Int
sealed trait IntList

case class ILCons(head: Int, tail: IntList) extends IntList

case class ILNil() extends IntList

def sum(l: IntList): Int = l match {
  case ILCons(h, t) => h + sum(t)
  case _ => 0
}

println(sum(ILCons(10, ILCons(20, ILCons(30, ILNil())))))