package u03lab.code.compose

case object Compose {
  val compose : (Int => Int, Int => Int) => Int => Int = (f, g) => a => f(g(a))

  // an alternative solution
  //  val compose = (f: Int => Int, g: Int => Int) => (a: Int) => f(g(a))
}

case object UseCompose extends App {
  println(Compose.compose(_+1,_*2)(5))
}