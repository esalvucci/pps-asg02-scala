package u03lab.code.fibonacci

case object Fibonacci {
  def get(n: Int): Int = {
    @annotation.tailrec
    def loop(n: Int, prev: Int, cur: Int): Int = n match {
      case n if n == 0 => prev
      case _ => loop(n - 1, cur, cur + prev)
    }

    loop(n, 0, 1)
  }
}

object UseFibonacci extends App {
  println(Fibonacci.get(5))
  println(Fibonacci.get(10))
}


/*
 non tailrec recursion!!!
val fibonacci : Int => Int = {
case n if n > 1 => fibonacci(n-1)+fibonacci(n-2)
case 0 => 0
case _ => 1
}
*/
