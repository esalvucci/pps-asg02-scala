package u03lab.code.advancedList

import u03lab.code.list.MyList
import u03lab.code.list.MyList.{Cons, Nil}

sealed trait Person
case class Student(name: String, year: Int) extends Person
case class Teacher(name: String, course: String) extends Person

object PersonImpl extends Person {
  def name(p: Person): String = p match {
    case Student(n,_) => n
    case Teacher(n,_) => n
  }
}

sealed trait Either[+E, +R]
case class Left[+E](error: E) extends Either[E, Nothing]
case class Right[+R](response: R) extends Either[Nothing, R]

object AdvancedExercise extends App {

  def getCoursesFromTeachers(list: MyList[Person]): MyList[String] = {
    def loop(l: MyList[Person]): MyList[String] = l match {
      case Cons(h, t) => h match {
        case Teacher(name, course) => Cons(course, loop(t))
        case Student(_, _) => loop(t)
      }
      case Nil() => Nil()
    }
    loop(list)
  }

  def filter[A](value: Option[A])(predicate: A => Boolean): Option[A] = {
    if (predicate(value.get)) value
    else None
  }

  def map[A](value: Option[A])(predicate: A => Boolean): Option[Boolean] = value match {
    case Some(_) => Some(predicate(value.get))
    case _ => None
  }

  def map2[A, B, C](firstValue: Option[A], secondValue: Option[B])(function: (A, B) => C): Option[C] = (firstValue, secondValue) match {
    case (Some(a), Some(b)) => Some(function(a, b))
    case _ => None
  }

  def foldRight[A, B](list: MyList[A])(default: B)(f: (A, B) => B): B = list match {
    case Nil() => default
    case Cons(h, t) => f(h, foldRight(t)(default)(f))
  }

  def foldLeft[A, B](list: MyList[A])(default: B)(f: (A, B) => B): B = list match {
    case Nil() => default
    case Cons(h, t) => foldLeft(t)(f(h, default))(f)
  }

  val peopleList: MyList[Person] = Cons(Student("alice", 2018), Cons(Teacher("bob", "CC"),
                             Cons(Teacher("charl", "DD"), Cons(Student("daniel", 2018), Cons(Teacher("enry", "EE"), Nil())))))
  System.out.println(getCoursesFromTeachers(peopleList))

  System.out.println(filter(Some(5))(_ > 2))
  System.out.println(filter(Some(5))(_ > 8))

  System.out.println(map(Some(5))(_ > 2))

  val integersList: MyList[Int] = Cons(3,Cons(7,Cons(1,Cons(5, Nil()))))
  System.out.println("foldRight => " + foldRight(integersList)(0)(_+_))
  System.out.println("foldLeft => " + foldLeft(integersList)(0)(_+_))

  System.out.println("foldRight => " + foldRight(integersList)("")(_+_))
  System.out.println("foldLeft => " + foldLeft(integersList)("")(_+_))

  def mean(xs: IndexedSeq[Int]): Either[String, Double] =
    if (xs.isEmpty)
      Left("mean of empty list!")
    else
      Right(xs.sum / xs.length)

  val nums = IndexedSeq(1, 2, 3)
  val emptyNums = IndexedSeq()
  println(mean(nums))
  println(mean(emptyNums))
}