package extra

import scala.annotation.tailrec

/**
  * This esercise is en extraxt of some exercises
  * in S-99: Ninety-Nine Scala Problems
  *
  * http://aperiodic.net/phil/scala/s-99/
  */
object MyListConvertion {

  implicit class MyList[A](val list: List[A]) {
    def last: A = list match {
      case head :: tail => if (tail == Nil) head else last
      case _ => throw new NoSuchElementException
    }

    def penultimate: A = {
      @tailrec
      def loop(l: List[A]): A = l match {
        case head :: _ :: Nil => head
        case _ :: tail => loop(tail)
        case _ => throw new NoSuchElementException
      }

      loop(list)
    }

    def nth(n: Int): Option[A] = {
      @tailrec
      def loop(l: List[A], index: Int): Option[A] = l match {
        case head :: tail => if (n == index) Some(head) else loop(tail, index + 1)
        case Nil => Option.empty
      }

      loop(list, 0)
    }

    def lenght: Int = {
      @tailrec
      def loop(l: List[A], currentLenght: Int): Int = l match {
        case head :: tail => loop(tail, currentLenght + 1)
        case Nil => currentLenght
      }

      loop(list, 0)
    }

    def reverse: List[A] = {
      @tailrec
      def loop(result: List[A], currentList: List[A]): List[A] = currentList match {
        case Nil => result
        case head :: tail => loop(head :: result, tail)
      }

      loop(Nil, list)
    }

    def isPalindrome: Boolean = list == list.reverse

    def compress: List[A] = {
      def loop(l: List[A]): List[A] = l match {
        case h :: t => if (t.nth(0).isDefined && h == t.nth(0).get) loop(t)
        else h :: loop(t)
        case _ => l
      }

      loop(list)
    }

  }
}

object MyList extends App {

  import MyListConvertion._

  println(List(1, 2, 3, 4, 5, 6).last)
  println(List(1, 2, 3, 4, 5, 6).penultimate)
  println(List(1, 2, 3, 4, 5, 6).nth(2))
  println(List(1, 2, 3, 4, 5, 6).length)
  println(List(1, 2, 3, 4, 5, 6).reverse)
  println(List(1, 2, 3, 4, 3, 2, 1).isPalindrome)
  println(List(1, 1, 2, 2, 2, 3, 4, 2, 1, 3, 3, 4).compress)
  println(implicitly(MyListConvertion))
}
