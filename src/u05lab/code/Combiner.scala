package u05lab.code

/** Implement trait Functions with an object FunctionsImpl such that the code
  * in TryFunctions works correctly. To apply DRY principle at the best,
  * note the three methods in Functions do something similar.
  * Use the following approach:
  * - find three implementations of Combiner that tell (for sum,concat and max) how
  *   to combine two elements, and what to return when the input list is empty
  * - implement in FunctionsImpl a single method combiner that, other than
  *   the collection of A, takes a Combiner as input
  * - implement the three methods by simply calling combiner
  *
  * When all works, note we completely avoided duplications..
 */

trait Combiner[A] {
  def unit: A
  def combine(a: Seq[A]): A
}

trait Functions {
  def combine[A](a: Seq[A])(implicit combiner: Combiner[A]): A
  def sum(a: Seq[Double]): Double
  def concat(a: Seq[String]): String
  def max(a: Seq[Int]): Int
}

object FunctionsImpl extends Functions {

  implicit val doubleCombiner: Combiner[Double] = new Combiner[Double] {
    override def unit: Double = 0.0
    override def combine(a: Seq[Double]): Double = a.foldRight(unit)(_ + _)
  }

  implicit val stringCombiner: Combiner[String] = new Combiner[String] {
    override def unit: String = ""
    override def combine(a: Seq[String]): String = a.foldRight(unit)(_ + _)
  }

  implicit val intCombiner: Combiner[Int] = new Combiner[Int] {
    override def unit: Int = Int.MinValue
    override def combine(a: Seq[Int]): Int = a match {
      case head :: tail => if (head > combine(tail)) head else combine(tail)
      case _ => unit
    }
  }

  def combine[A](a: Seq[A])(implicit combiner: Combiner[A]): A = combiner.combine(a)

  override def sum(a: Seq[Double]): Double = combine(a)
  override def concat(a: Seq[String]): String = combine(a)
  override def max(a: Seq[Int]): Int = combine(a)
}

object TryFunctions extends App {
  val f: Functions = FunctionsImpl
  println(f.sum(List(10.0,20.0,30.1))) // 60.1
  println(f.sum(List()))                // 0.0
  println(f.concat(Seq("a","b","c")))   // abc
  println(f.concat(Seq()))              // ""
  println(f.max(List(-10,3,-5,0)))      // 3
  println(f.max(List()))                // -2147483648
}